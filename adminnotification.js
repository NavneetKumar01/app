function enablermname(){
    document.getElementById("rmname").disabled=false;
}
function disablermname(){
    document.getElementById("rmname").disabled=true;
    document.getElementById("querytype").disabled=true;
    disablequery();
    disablefields();
}
//enabling query
function enablequery(){
    document.getElementById("querytype").disabled=false;
}

function disablequery(){
    document.getElementById("querytype").disabled=true;
    disablefields();
}

//diabling showfields

function disablefields(){
    document.getElementById("date").style.display = "none";
    document.getElementById("datelabel").style.display = "none";
    document.getElementById("description").style.display = "none";
    document.getElementById("deslabel").style.display = "none";
    document.getElementById("submit").style.display = "none";
}

//showing different fields
function showbirthday(){
    document.getElementById("date").style.display = "block";
    document.getElementById("datelabel").style.display = "block";
    document.getElementById("description").style.display = "block";
    document.getElementById("deslabel").style.display = "block";
    document.getElementById("submit").style.display = "block";
}